package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
        System.out.println("Let's play round "+ roundCounter);
        String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
        String computerChoice = computerChoose();
        String choiceString = "Human chose " + humanChoice +", computer chose " +computerChoice+".";

        
        if(isWinner(humanChoice, computerChoice)) {
            System.out.println(choiceString + "Human wins.");
            humanScore += 1;
        }
        else if (isWinner(computerChoice, humanChoice)) {
            System.out.println(choiceString + "Computer wins.");
            computerScore += 1;
        } 
        else{
            System.out.println(choiceString + "It's a tie.");
        }
        
        System.out.println("Score: me" + computerScore + ", you " +humanScore);
        roundCounter += 1;

        String continueAnswer = continuePlaying();
        if (continueAnswer.equals("n")) {
            break;
        }
    

    }
    System.out.println("Bye bye :)");

       
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public String computerChoose() {
        Random random = new Random();
        String choice = rpsChoices.get(random.nextInt(2)); 
        return choice;
    }

    public boolean isWinner(String choice1, String choice2){
        if(choice1.equals("Paper")) {
            return choice2.equals("rock");
        }
        else if(choice1.equals("Scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");}
    }

    public String continuePlaying() {
        while (true){
            String continueAnswer = readInput("Continue (y/n)?");
            if (validateInput(continueAnswer, Arrays.asList("y", "n")) {
                return continueAnswer;
            } 
            else {System.out.println("I don't understand "+ continueAnswer+". Try again");}
        }


    }

    public boolean validateInput(String input, String validInput) {
        input = input.toLowerCase();
        return validInput.includes(input);
    }

}
